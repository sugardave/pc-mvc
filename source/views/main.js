enyo.ready(function () {

	enyo.kind({
		name: "PropertyCross.MainView",
		kind: enyo.Panels,
		fit: true,
		draggable: false,
		controller: ".app.controllers.panels",
		bindings: [
			{from: ".controller.index", to: ".index", oneWay: false},
			{from: ".selectPanelByName", to: ".controller.selectPanelByName", transform: "bindMethod"}
		],
		components: [
			{name: "search", kind: "PropertyCross.SearchPanel"},
			{name: "favorites", kind: "PropertyCross.FavoritesPanel"},
			{name: "results", kind: "PropertyCross.ResultsPanel"},
			{name: "listing", kind: "PropertyCross.ListingPanel"}
		],
		bindMethod: function(inMethod) {
			return this.bindSafely(inMethod);
		}
	});
	
});
