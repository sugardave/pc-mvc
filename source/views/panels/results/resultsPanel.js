enyo.ready(function() {

	enyo.kind({
		name: "PropertyCross.ResultsPanel",
		kind: "PropertyCross.Panel",
		classes: "results",
		components: [
			{kind: "onyx.Toolbar", components: [
				{name: "resultsHeader", content: "X of X matches", classes: "header-center"},
				{kind: "onyx.Button", content: "Back", classes: "header-button-left", ontap: "goBack"}
			]},
			{name: "resultsError", kind: "onyx.Drawer", open: false, classes: "panel-row error-drawer", components: [
				{name: "resultsErrorContent", content: "There was a problem loading the listings."}
			]},
			{name: "resultsBox", kind: "onyx.Groupbox", classes: "panel-row", fit: true, layoutKind:"FittableRowsLayout", components: [
				{kind: "onyx.GroupboxHeader", content: "Found locations"},
				/*{kind: enyo.Scroller, fit: true, touch: true, horizontal: "hidden", components: [
					{name: "resultsList", kind: "wip.Repeater", controllerFoo: ".app.controllers.results", onItemTap: "resultsListItemTap", components: [
						{name: "resultItem", kind: "PropertyCross.ListingItem"}
					]}
				]}*/
				{kind: enyo.DataList, fit: true, touch: true, controller: ".app.controllers.searchResults", components: [
				//{kind: enyo.DataRepeater, fit: true, touch: true, controller: ".app.controllers.searchResults", components: [
					{nameX: "resultItem", kind: "PropertyCross.ListingItem"}
					//{kind: enyo.Image, bindFrom: "thumb_url", bindTo: ".src"}
				]}
			]},
			{name: "moreDrawer", kind: "onyx.Drawer", open: false, components: [
				{name: "moreButton", kind: "onyx.Button", style: "display:block;margin-left:20px;margin-bottom:20px;", showing: false, content: "Load more...", onclick: "getMoreListings"}
			]},

			{name: "loadingPopup", kind: "PropertyCross.MessagePopup", message: "Loading..."}
		],
		transformPrice: function() {
			this.log("HMMM");
		}
	});

});