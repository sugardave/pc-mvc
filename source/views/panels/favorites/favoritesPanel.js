enyo.ready(function() {

	enyo.kind({
		name: "PropertyCross.FavoritesPanel",
		kind: "PropertyCross.Panel",
		classes: "favorites",
		components: [
			{kind: onyx.Toolbar, components: [
				{content: "Favorites", classes: "header-center"},
				{kind: onyx.Button, content: "Back", classes: "header-button-left", ontap: "goBack"}
			]}
		]
	});

});