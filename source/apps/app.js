enyo.kind({
	name: "PropertyCross.Application",
	kind: enyo.Application,
	controllers: [
		{name: "storage", kind: "PropertyCross.StorageController"},
		{name: "panels", kind: "PropertyCross.ViewController"},
		{name: "searchResults", kind: "PropertyCross.ListingsCollection", model: "PropertyCross.ListingModel"}
	],
	view: "PropertyCross.MainView",
	goBack: function() {
		this.controllers.panels.selectPanelByName(this.controllers.panels.get("goBack"));
	},
	showPanel: function(inSender, inEvent) {
		this.log(arguments);
		var originator = inEvent.originator;
		var panel = originator.panel;
		var goBack = originator.goBack;
		if (panel) {
			if (goBack) {
				this.controllers.panels.set("goBack", goBack);
			}
			this.controllers.panels.selectPanelByName(panel);
		}
	}
});
