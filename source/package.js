enyo.depends(
	// include support libraries
	"$lib/layout",
	"$lib/onyx",
	// include application sources
	"css",
	"ext",
	"models",
	"controllers",
	"views",
	"apps",
	// include our default entry point
	"start.js"
);
